package com.radmas.iycp

import android.content.Context
import com.radmas.analytics.AnalyticsConfig
import com.radmas.android_base.AppConfig
import com.radmas.android_base.domain.model.AppType.Companion.buildForValue
import com.radmas.core.domain.model.DeleteAccountType
import com.radmas.core.domain.model.EmergencyPhone
import com.radmas.core.domain.model.LegalTextType
import com.radmas.core.domain.model.LoginType
import com.radmas.core.infrastructure.toLocale
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppConfiguration @Inject internal constructor(
    private val context: Context,
) : AppConfig {
    private val resources get() = context.resources

    override val buildType = BuildConfig.BUILD_TYPE

    override val isDebug = BuildConfig.DEBUG

    override val appType = buildForValue(resources.getString(R.string.app_type))

    override val baseUrl = resources.getString(R.string.base_url)

    override val clientId = resources.getString(R.string.client_id)

    override val appKey = resources.getString(R.string.app_key)

    override val appVersion = BuildConfig.VERSION_NAME

    override val youtubeApiKey = ""

    override val googleAuthApiKey = resources.getString(R.string.google_client_id)

    override val thirdPartyNotificationKey = resources.getString(R.string.gcm_defaultSenderId)

    override val loginType = LoginType.valueOf(resources.getString(R.string.login_type))

    override val externalLoginUrl = ""

    override val legalTextType =
        LegalTextType.valueOf(resources.getString(R.string.legal_texts_type))

    override val hasFaq = false

    override val allowsSharing = false

    override val allowsRating = false

    override val emergencyPhones = listOf<EmergencyPhone>()

    override val hasChangePasswordAction = true

    override val shouldAskForGPSActivation = false

    override val analyticsConfig = AnalyticsConfig.NONE

    override val analyticsToken: String? = null

    override val showEmergencyDialog = false

    override val enableRequestWithOrder = true

    override val enableEditRequest = false

    override val predeterminedResponsesEnabled = false

    override val offlineModeEnabled = false

    override val downloadRequestNewEndpoint = false

    override val offlineFilterEnabled = false

    override val offlineRequestProcessing = false

    override val openIdRedirectUri = ""

    override val enableBaseMapLayers = false

    override val packageName = context.packageName

    override val poiLimit = 0

    override val faqUrl = ""

    override val hasGeofences = false

    override val enableAutoShowRating = false

    override val hasLegalTexts = false

    override val hasPrivacyPolicy = false

    override val hasProfileEdition = false

    override val signingHash = ""

    override val hasCustomManualActions = false

    override val bioAuthEnabled = false

    override val bioLockEnabled = false

    override val onlyMyAreaCounts = false

    override val deleteAccountType = DeleteAccountType.NONE

    override val mapProjection = 4326

    override val enableExtraPoiComposeFunctions = false

    override val enablePoiQrSearcher = false

    override val enableLongPressLogin = false

    override val enableImagesByPermissions = false

    override val composeRequestCreation = false

    override val enableCityJurisdictionElementsByService = false

    override val composeMyRequests = false

    override val enabledLocales: List<Locale> = listOf("es".toLocale())

    override val directLoginEnabled = false

    override val loginDeepLink = ""

    override val hasChannelSelection = false

    override val isInMarketPlace = false
}
