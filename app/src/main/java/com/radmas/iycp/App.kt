package com.radmas.iycp

import com.radmas.core.ui.app.BaseApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : BaseApplication()