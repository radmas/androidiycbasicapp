package com.radmas.iycp

import android.content.Context
import androidx.compose.runtime.Composable
import com.radmas.core.di.AppModule
import com.radmas.core.domain.model.ButtonAction
import com.radmas.core.domain.model.HomeElementConfig
import com.radmas.core.ui.HomeElementViewModel
import com.radmas.core.ui.UiContract
import com.radmas.core.ui.UiEvent
import com.radmas.core.ui.UiState
import com.radmas.core.ui.extensions.injectViewModel
import com.radmas.core.ui.screens.home.cards.HomeCardView
import com.radmas.core.ui.theme.components.AppBasicCardLayout
import com.radmas.iycp.FakeCardViewContract.Event
import com.radmas.iycp.FakeCardViewContract.State
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import javax.inject.Inject
import javax.inject.Singleton
import com.radmas.core.ui.utils.RequireUser

@InstallIn(SingletonComponent::class)
@Module
interface FakeCardDaggerModule {
    @Binds
    @IntoSet
    fun bindFakeCardAppModule(appModule: FakeCardAppModule): AppModule
}

@Singleton
class FakeCardAppModule @Inject constructor() : AppModule(
    homeElementViews = mapOf(
        Launcher.type to FakeCardView,
    )
)

object FakeCardView : HomeCardView<State, Event>() {

    @Composable
    override fun CardContent(
        state: State,
        onEvent: (Event) -> Unit,
        homeElementConfig: HomeElementConfig,
        loading: Boolean,
    ) {
        AppBasicCardLayout(
            homeElementConfig = homeElementConfig,
            action = ButtonAction(
                text = "Launch MyModule",
                enabled = !loading,
            ) {
                onEvent(Event.OnClick)
            },
        )
    }

    @Composable
    override fun viewModel(homeElementConfig: HomeElementConfig) =
        injectViewModel<FakeCardViewModel>()
}

object FakeCardViewContract : UiContract {

    data object State : UiState

    sealed interface Event : UiEvent {
        data object OnClick : Event
    }
}

@HiltViewModel
class FakeCardViewModel @Inject constructor(
    @ApplicationContext private val context: Context,
    private val requireUser: RequireUser,
) : HomeElementViewModel<State, Event>() {

    override fun createInitialState() = State

    override fun onEvent(
        homeElementConfig: HomeElementConfig, event: Event
    ) = when (event) {
        is Event.OnClick -> {
            requireUser(
                appViewModel = this,
                onUserLogged = { user ->
                    Launcher.launch(context, user.email.orEmpty())
                },
            )
        }
    }
}