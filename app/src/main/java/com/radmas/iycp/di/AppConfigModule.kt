package com.radmas.iycp.di

import com.radmas.android_base.AppConfig
import com.radmas.iycp.AppConfiguration
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
internal interface AppConfigModule {

    @Singleton
    @Binds
    fun providePresentationAppConfiguration(appConfiguration: AppConfiguration): AppConfig
}