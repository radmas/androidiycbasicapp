package com.radmas.iycp

import android.content.Context
import android.widget.Toast
import com.mycompany.mymodule.MyModuleEntryPoint
import com.radmas.android_base.domain.model.CardType

object Launcher {
    val type = CardType.FAKE_CARD

    fun launch(context: Context, email: String) {
        MyModuleEntryPoint.launchMyModuleActivity(
            context = context,
            param1 = email,
            callback1 = Toast.makeText(
                context,
                "callback1",
                Toast.LENGTH_SHORT
            )::show
        )
    }
}