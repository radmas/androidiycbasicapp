# Guia de uso del SDK de MTX 

![](https://img.shields.io/badge/version-3.1.0-green)

## Preparación del proyecto

### Paso 1: Descarga la aplicación de ejemplo

Clona el proyecto desde [aquí](https://bitbucket.org/radmas/androidiycbasicapp/src/master/)

Abre el proyecto recién clonado en Android Studio.

### Paso 2: Configura la aplicación de ejemplo

Debes establecer el `client_id` y la `base_url` en el archivo `src/main/res/values/app_config.xml` con los valores que te proporcionaremos:

```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    ...
    <string name="base_url">__YOUR_BASE_URL__</string>
    <string name="client_id">__YOUR_CLIENT_ID__</string>
    ...
</resources>
```

### Paso 3: Configura tus credenciales de Artifactory

1. Entra en [http://ci.mtx.tools:8081/](http://ci.mtx.tools:8081/)
2. Haz login con el usuario+contraseña que te proporcionamos.
3. Entra en el perfil (accesible desde el nombre de usuario arriba a la derecha).
4. Desbloquealo usando la misma contraseña del perfil y pulsa "unlock".
5. Cambia la contraseña.
6. Una vez cambiada, copia la "Encrypted Password" que aparece más abajo, pulsando en el botón.
7. Abre/Crea el archivo `gradle.properties` de tu home **(no en el del proyecto)**
    + En linux/mac: `~/.gradle/gradle.properties`
    + En windows: `C:\Users\username\.gradle\gradle.properties`
8. Rellena con el contenido siguiente:
```properties
   radmas_artifactory_username=<TU_PROPIO_USERNAME>
   radmas_artifactory_password=<TU_ENCRYPTED_PASSWORD>
   radmas_artifactory_url=http://ci.mtx.tools:8081/artifactory
   radmas_artifactory_release_repo=libs-release-local
   radmas_artifactory_snapshot_repo=libs-snapshot-local
```

### Paso 4: Ejecuta la aplicación de prueba

Después de ejecutar los pasos previos puedes compilar y ejecutar `app` como una aplicación de Android de modo habitual. Una vez esté ejecutando, podrás hacer login con un usuario que te proporcionaremos (algunas aplicaciones permiten la creación de usuarios nuevos o utilizar usuarios anónimos).

### Paso 5: Revisa el código fuente proporcionado

El código proporcionado se divide en dos partes:
1. `app` : Es una aplicación Android, parecida a la que se usará en producción, con los plugins necesarios para que funcione el SDK de MTX. El código perteneciente a `app` **no debe ser modificado**, salvo que en esta documentación se indique expresamente lo contrario, puesto que **no es la aplicación final**.
2. `mycardimpl` Un módulo (con tarjeta). Esta forma de integración ya no está soportada.
3. `mymodule` Esta documentación explica como crear un módulo similar a este. Es recomendable tomarlo como guía.

## Creación de un módulo propio

### Paso 1: Crea una nueva librería de Android

En Android Studio, el menú `File -> New -> New Module -> Android Library`, sigue las instrucciones del asistente (`mymodule` en los ejemplos).

Consulta cómo crear una librería en la [documentación oficial](https://developer.android.com/studio/projects/android-library).

Al realizar esta acción, Android Studio modificará algunos ficheros de forma automática. 
Debes revertir los cambios en cualquier fichero que no esté contenido en el módulo recién creado.

Todas las acciones indicadas más adelante se refieren al módulo recién creado (`mymodule` en los ejemplos), salvo que se especifique lo contrario.

### Paso 2: Añade tu librería

En el archivo `settings.gradle` del proyecto, incluye `mymodule`. 
Este cambio es temporal. Más adelante se indica como publicar una librería para que pueda ser integrada en las aplicaciones de MTX.

```kotlin
    include(
      //...
      ":mymodule",
    )
```

Incluye tu nueva librería como dependencia de `app` en su archivo `app/build.gradle`:

```groovy
    dependencies {
       // ...
       implementation(project(":mymodule"))
       // ...
    }
```

### Paso 3: Configura tu librería

Edita el fichero `mymodule/build.gradle.kts`, incluye tu propio namespace y tus dependencias:

```kotlin
    android {
       namespace = "com.mycompany.mymodule"
    
       //...
    }
```
Sincroniza gradle y se descargarán las dependencias necesarias.

### Paso 3: Crea una activity inicial y un punto de entrada

Crea un activity (en el ejemplo `MyModuleActivity`) con una vista.

Crea un punto de entrada con una función con primer parámetro
de tipo `android.content.Context`.

Esta función será invocada por la app real. el contexto que se proporcionará será de aplicación, 
por lo que es importante incluir el flag `FLAG_ACTIVITY_NEW_TASK`

```kotlin
    object MyModuleEntryPoint {
      fun launchMyModuleActivity(context: Context, /* other params*/) {
         val intent = Intent(context, MyModuleActivity::class.java)
         // ...
         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
         context.startActivity(intent)
      }
   }
```

### Paso 4: Prueba tu módulo

Se proporciona el objeto `com.radmas.iycp.Launcher` que permite lanzar desde una tarjeta cualquier activity.
Este launcher NO forma parte de la integración, pero es muy similar a como realizaremos la integración real. 

Edita su contenido para poder realizar pruebas, pero ten en cuenta que este fichero no formará parte de tu módulo.

Cambia el type y el contenido del metodo action para que llame a tu punto de entrada.

```kotlin
   object Launcher {
       val type = CardType.YOUR_CARD_TYPE
   
       fun launch(context: Context) {
          MyModuleEntryPoint.launchMyModuleActivity(
               context = context,
               //...
           )
       }
   }
```

donde `YOUR_CARD_TYPE` corresponde a un caso del enumerado `com.radmas.android_base.domain.model.CardType` que nuestro equipo te indicará para tu tarjeta. Puedes usar `CardType.CARD_TYPE_8` si todavía no te hemos proporcionado un tipo.

Lanza la app y comprueba que al pulsar en el boton "Launch MyModule" se lanza correctamente tu activity.

### Paso 5: Completa tu módulo

Expande la funcionalidad de tu módulo con tu propia lógica. 
Consulta el módulo `mymodule` proporcionado como ejemplo si te surgen dudas.

### Paso 6: Publica tu módulo

Publica tu librería en formato `aar` para que pueda ser declarada como dependencia en nuestra app,
en cualquier repositorio de artefactos compatible con gradle. 

Puedes ver un ejemplo de como hacerlo usando los plugins de `maven-publish` 
y `com.jfrog.artifactory`en el fichero `build.gradle.kts` de `mymodule`.

Puedes consultar más información sobre el plugin de maven en su [documentación oficial](https://docs.gradle.org/current/userguide/publishing_maven.html).
Puedes consultar más información sobre el plugin de artifactory en su [documentación oficial](https://github.com/jfrog/artifactory-gradle-plugin).


## Tipos de aplicación

El SDK proporciona una base de código que puede ser usada para varios tipos de aplicaciones.

La aplicacion proporcionada viene configurada para funcionar como una app interna.

Si deseas que funcione como una app externa, 
debes modificar los campos `login_type` y `app_type` en el archivo
`src/main/res/values/app_config.xml`:

```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    ...
    <string name="login_type">CITY</string>
    <string name="app_type">cityapp</string>
    ...
</resources>
```

Además es posible cambiar algunos de los otros valores de este archivo para obtener la configuración deseada.

Te indicaremos estos valores concretos dependiendo de la integración.

## Compatibilidad

El SDK es compatible con Java 21 o superior y con Kotlin 2.0.0 o superior.

El minSDK de Android del módulo debe ser inferior o igual a 27