@file:Suppress("LocalVariableName", "VariableNaming")

include(
    ":app",
    ":mymodule",
)

settings.pluginManagement {
    val radmas_artifactory_mtx_base_version: String by settings

    apply(from = "radmas_artifactory.gradle.kts")
    val radmasRepos: RepositoryHandler.() -> Unit by settings

    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()

        radmasRepos()
    }

    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("com.radmas.gradle.plugins")) {
                useVersion(radmas_artifactory_mtx_base_version)
            }
        }
    }
}

apply(from = "radmas_artifactory.gradle.kts")
val radmasRepos: RepositoryHandler.() -> Unit by settings

settings.dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven(url = "https://jitpack.io")
        radmasRepos()
    }
}