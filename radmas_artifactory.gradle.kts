val radmas_artifactory_url: String by extra
val radmas_artifactory_release_repo: String by extra
val radmas_artifactory_snapshot_repo: String by extra
val radmas_artifactory_username: String by extra
val radmas_artifactory_password: String by extra

fun RepositoryHandler.buildRepo(name: String, isRelease: Boolean) {
    maven {
        url = uri("$radmas_artifactory_url/$name")
        isAllowInsecureProtocol = true
        credentials {
            username = radmas_artifactory_username
            password = radmas_artifactory_password
        }
    }
}

fun RepositoryHandler.radmasReleaseRepo() = buildRepo(radmas_artifactory_release_repo, true)
fun RepositoryHandler.radmasSnapshotRepo() = buildRepo(radmas_artifactory_snapshot_repo, false)

val radmasReposFor: RepositoryHandler.(version: String) -> Unit by extra { delegate: RepositoryHandler, version: String ->
    if (version.endsWith("SNAPSHOT")) {
        delegate.radmasSnapshotRepo()
    } else {
        delegate.radmasReleaseRepo()
    }
}

val radmasRepos: RepositoryHandler.() -> Unit by extra { delegate: RepositoryHandler ->
    delegate.radmasSnapshotRepo()
    delegate.radmasReleaseRepo()
}