import org.jfrog.gradle.plugin.artifactory.dsl.ArtifactoryPluginConvention

plugins {
    id("kotlin-android")
    id("com.android.library")
    id("org.jetbrains.kotlin.plugin.compose")
    `maven-publish`
    id("com.jfrog.artifactory")
}

val domain = "com.mycompany"
val moduleName = "mymodule"
val libraryVersion = "1.0.1-SNAPSHOT"

val publicationName = "aar"
val componentName = "release"

android {
    namespace = "$domain.$moduleName"

    group = "$domain.$moduleName"
    version = libraryVersion

    compileSdk = 35
    defaultConfig {
        minSdk = 27
    }

    java {
        toolchain {
            languageVersion = JavaLanguageVersion.of(21)
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
            )
            consumerProguardFiles("consumer-rules.pro")
        }
    }

    publishing {
        singleVariant(componentName)
    }

    buildFeatures {
        compose = true
    }
}

publishing {
    publications {
        register<MavenPublication>(publicationName) {
            groupId = domain
            artifactId = moduleName
            version = libraryVersion

            afterEvaluate {
                from(components.getByName(componentName))
            }
        }
    }
}

configure<ArtifactoryPluginConvention> {
    publish {
        contextUrl = extra["radmas_artifactory_url"] as? String
        repository {
            repoKey = if (libraryVersion.endsWith("SNAPSHOT")) {
                extra["radmas_artifactory_snapshot_repo"] as? String
            } else {
                extra["radmas_artifactory_release_repo"] as? String
            }
            username = extra["radmas_artifactory_username"] as? String
            password = extra["radmas_artifactory_password"] as? String
        }

        defaults {
            publications(publicationName)
        }
    }
}

// publish with ./gradlew :mymodule:artifactoryPublish

dependencies {
    implementation(platform("androidx.compose:compose-bom:2024.12.01"))
    implementation("androidx.compose.foundation:foundation")
    implementation("androidx.activity:activity-compose:1.9.3")
}