package com.mycompany.mymodule

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.text.BasicText
import androidx.compose.ui.Modifier

class MyModuleActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val param1 = intent.getStringExtra("param1").orEmpty()

        setContent {
            BasicText(
                text = "param1: $param1",
                modifier = Modifier.clickable {
                    MyModuleEntryPoint.callback1?.invoke()
                }
            )
        }
    }
}