package com.mycompany.mymodule

import android.content.Context
import android.content.Intent

object MyModuleEntryPoint {
    var callback1: (() -> Unit)? = null

    fun launchMyModuleActivity(context: Context, param1: String, callback1: () -> Unit) {
        this.callback1 = callback1
        val intent = Intent(context, MyModuleActivity::class.java)
        intent.putExtra("param1", param1)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }
}

